/*
 * Copyright 2022 Rafael González Centeno.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.daw1;

/**
 *
 * @author Rafael González Centeno
 */
public class ExceptionTest05 {
    public static void main(String[] args) {
        java.util.Scanner sc = new java.util.Scanner(System.in);
        int nums[] = new int[4];
        int opc = 2;
        do{
            try{
                System.out.println("Generar excepción? \n1.- Sí\n2.- No\n");
                opc = sc.nextInt();
                if(opc == 1){
                    nums[7] = 10;
                }
            }catch(ArrayIndexOutOfBoundsException ex){
                System.out.println("Posición de array inexistente. " + ex.getMessage());
            }
            catch(Exception ex){
                System.out.println("Algo inesperado ha ocurrido");
            }
        }while(opc != 3);
    }
}
