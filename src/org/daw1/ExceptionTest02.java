/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.daw1;

/**
 *
 * @author Rafael González Centeno
 */
public class ExceptionTest02 {
    public static void main(String[] args) {
        try{
            GeneraExcepcion.genException();
        }
        catch(ArrayIndexOutOfBoundsException e){
            System.out.println("Posición inexistente: " + e.getMessage());
        }
        System.out.println("Código después de generarse la excepción");
    }
}
