/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.daw1;

/**
 *
 * @author Rafael González Centeno
 */
public class ExceptionTest01 {
    public static void main(String[] args) {
        int nums[] = new int[4];
        System.out.println("Código antes de generarse la excepción");
        try{
            nums[7] = 10;
            System.out.println("Código que nunca se ejecuta");
        }
        catch(ArrayIndexOutOfBoundsException e){
            System.out.println("Posición inexistente: " + e.getMessage());
        }
        System.out.println("Código después de generarse la excepción");
    }
}
