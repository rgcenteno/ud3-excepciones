/*
 * Copyright 2022 Rafael González Centeno.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.daw1;

/**
 *
 * @author Rafael González Centeno
 */
public class ExceptionTest04 {
    public static void main(String[] args) {
        int nums[] = new int[4];
        System.out.println("Código antes de generarse la excepción");
        try{
            nums[7] = 10;
            System.out.println("Código que nunca se ejecuta");
        }
        catch(ArithmeticException e){
            System.out.println("Posición inexistente: " + e.getMessage());
        }
        System.out.println("Código después de generarse la excepción");
    }
}
