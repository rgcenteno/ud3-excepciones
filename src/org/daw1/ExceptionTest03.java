
package org.daw1;

/**
 *
 * @author Rafael González Centeno
 */
public class ExceptionTest03 {
    public static void main(String[] args) {
        java.util.Scanner sc = new java.util.Scanner(System.in);
        char opc;
        do{
            System.out.println("Generar excepción? s/n");
            opc = sc.next().charAt(0);
            if(opc == 's'){
                GeneraExcepcion.genException();
                System.out.println("Código después de generar la excepción");
            }
        }while(opc == 's');
    }
    
}
